## Instructions
Each of you will be assigned three problem statements. Your task is to validate the problems, solve and generate test cases for them. You are to also tag each question with difficulty level (easy, medium, hard), and provide tags. See folder for problem 0. Size of each stdin/stdout should not exceed 5 MB.

To work on the problem statements simply fork [competitive-programming-contest](https://bitbucket.org/MrPiTimesE/competitive-programming-contest-mar-25), make modifications and give a pull request.

Feel free to use [test-case-generator](http://test-case-generator.herokuapp.com/).

Use redirection to generate

```
$ python solution.py < test_cases/2/in.txt > test_cases/2/out.txt

```


```
.
├── problem-statements
│   ├── 0
│   │   ├── difficulty.txt
│   │   ├── problem.md
│   │   ├── solution.py
│   │   ├── tags.txt
│   │   └── test_cases
│   │       ├── 1
│   │       │   ├── in.txt
│   │       │   └── out.txt
│   │       └── 2
│   │           ├── in.txt
│   │           └── out.txt
│   ├── 1
│   │   └── problem.md
│   ├── 10
│   │   └── problem.md
│   ├── 11
│   │   └── problem.md
│   ├── 12
│   │   └── problem.md
│   ├── 13
│   │   └── problem.md
│   ├── 14
│   │   └── problem.md
│   ├── 15
│   │   └── problem.md
│   ├── 16
│   │   └── problem.md
│   ├── 17
│   │   └── problem.md
│   ├── 18
│   │   └── problem.md
│   ├── 19
│   │   └── problem.md
│   ├── 2
│   │   └── problem.md
│   ├── 20
│   │   └── problem.md
│   ├── 21
│   │   └── problem.md
│   ├── 22
│   │   └── problem.md
│   ├── 23
│   │   └── problem.md
│   ├── 24
│   │   └── problem.md
│   ├── 3
│   │   └── problem.md
│   ├── 4
│   │   └── problem.md
│   ├── 5
│   │   └── problem.md
│   ├── 6
│   │   └── problem.md
│   ├── 7
│   │   └── problem.md
│   ├── 8
│   │   └── problem.md
│   └── 9
│       └── problem.md
└── readme.md


```