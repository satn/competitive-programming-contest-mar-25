### Problem Statement 18

The degrees of separation hypothesis states that any two people in the world can be connected via a chain of a friend of a friend statements by at most six steps.


Given a world of N people, find out the minimum shortest chain of a friend of a friend statements that connect two given people.


**Assumptions:**


You are a friend of yourself.

If A is a friend of B, then B is a friend of A. The chain between A and B is 1. That is that there is a single “friend of a friend” statement that connects to the two.


Another example:

If A is a friend of B, and B is a friend of C, then A and C are separated by two friend of a friend statements. So the expected output is 2.


Also assume that the name of each person is distinct.


**Input Format**

The first line of input consists of an integer T which is the number of test cases. Then the test cases follow. Each test case consists of N+2  lines of input where N is the number of people in the world. The format is as follows

N P<sub>0</sub> P<sub>1</sub> … P<sub>N-1</sub>

M<sub>0,0</sub> M<sub>0,1</sub> … M<sub>0,N-1</sub>

M<sub>1,0</sub> M<sub>1,1</sub> … M<sub>1,N-1</sub>

… … … …

… … … …

… … … …

M<sub>N-1,0</sub> M<sub>N-1,1</sub> … M<sub>N-1,N-1</sub>

P<sub>v</sub> P<sub>w</sub>


Here the M<sub>i,j</sub>s are all valued 1 or 0. The meaning of it is that if the person P<sub>i</sub> is friends with P<sub>j</sub> then M<sub>i,j</sub> = M<sub>j,i</sub> = 1; if they are not, then M<sub>i,j</sub> = M<sub>j,i</sub> = 0. Obviously, from the constraints of the problem, M<sub>i,i</sub> = 1 as every person is friends with themselves!


The idea is to find the shortest “friend of a friend path” between P<sub>v</sub> and P<sub>w</sub>. 


**Output Format**


For each test case, output an integer that represents the shortest “friend of a friend path” between P<sub>v</sub>and P<sub>w</sub>. (See the Assumption section to see what the integer represents). If P<sub>V</sub> and P<sub>w</sub> are NOT connected by any “friend of a friend path” then output N/A.

**Constraints**


1 <= T <=10000

2 <= N <= 1000000

Let the length of each name of each person P<sub>i</sub> be L<sub>i</sub>. Then


1 <= L<sub>i</sub> <= 10


**Sample Input**
```
1
5 Bharat Lori Kunal John Umber
1 1 1 0 0
1 1 0 0 1
1 0 1 0 1
0 0 0 1 0
0 1 1 0 1
Bharat Umber
```
**Sample Output**
```
2
```

